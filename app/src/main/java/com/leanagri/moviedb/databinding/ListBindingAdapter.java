package com.leanagri.moviedb.databinding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.leanagri.moviedb.data.remote.Resource;
import com.leanagri.moviedb.view.base.BaseAdapter;

import java.util.List;

/**
 * Created By mayurlathkar on 11,May,2019
 */
final class ListBindingAdapter {

    private ListBindingAdapter(){
        // Private Constructor to hide the implicit one
    }

    /**
     * Set resource.
     *
     * @param recyclerView the recycler view
     * @param resource     the resource
     */
    @SuppressWarnings("unchecked")
    @BindingAdapter(value = "resource")
    public static void setResource(RecyclerView recyclerView, Resource resource){
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if(adapter == null)
            return;

        if(resource == null || resource.data == null)
            return;

        if(adapter instanceof BaseAdapter){
            ((BaseAdapter)adapter).setData((List) resource.data);
        }
    }

}
