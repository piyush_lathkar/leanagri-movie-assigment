package com.leanagri.moviedb.di.builder;

import com.leanagri.moviedb.view.activity.MainActivity;
import com.leanagri.moviedb.view.activity.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Module
public abstract class ActivityBuilderModule {

    /**
     * Main activity main activity.
     *
     * @return the main activity
     */
    @SuppressWarnings("unused")
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract MainActivity mainActivity();

    @SuppressWarnings("unused")
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract SplashActivity splashActivity();

}
