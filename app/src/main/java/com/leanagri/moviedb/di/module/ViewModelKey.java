package com.leanagri.moviedb.di.module;

import android.arch.lifecycle.ViewModel;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import dagger.MapKey;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@MapKey
@interface ViewModelKey {
    /**
     * Value class.
     *
     * @return the class
     */
    @SuppressWarnings("unused")
    Class<? extends ViewModel> value();
}
