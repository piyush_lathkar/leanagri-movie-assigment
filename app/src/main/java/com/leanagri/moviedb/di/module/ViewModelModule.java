package com.leanagri.moviedb.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.leanagri.moviedb.viewmodel.MovieDetailsViewModel;
import com.leanagri.moviedb.viewmodel.MovieListViewModel;
import com.leanagri.moviedb.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Module
public abstract class ViewModelModule {

    /**
     * Binds movie list view model view model.
     *
     * @param movieListViewModel the movie list view model
     * @return the view model
     */
    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsMovieListViewModel(MovieListViewModel movieListViewModel);

    /**
     * Binds movie details view model view model.
     *
     * @param movieDetailsViewModel the movie details view model
     * @return the view model
     */
    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailsViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsMovieDetailsViewModel(MovieDetailsViewModel movieDetailsViewModel);


    /**
     * Binds view model factory view model provider . factory.
     *
     * @param viewModelFactory the view model factory
     * @return the view model provider . factory
     */
    @Binds
    @SuppressWarnings("unused")
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);

}
