package com.leanagri.moviedb.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.leanagri.moviedb.data.local.MovieDatabase;
import com.leanagri.moviedb.data.local.dao.MovieDao;
import com.leanagri.moviedb.data.remote.ApiConstants;
import com.leanagri.moviedb.data.remote.ApiService;
import com.leanagri.moviedb.data.remote.RequestInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Module(includes = ViewModelModule.class)
public class AppModule {

    /**
     * Provide ok http client ok http client.
     *
     * @return the ok http client
     */
    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(ApiConstants.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.readTimeout(ApiConstants.READ_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.writeTimeout(ApiConstants.WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.addInterceptor(new RequestInterceptor());
        okHttpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return okHttpClient.build();
    }

    /**
     * Provide retrofit api service.
     *
     * @param okHttpClient the ok http client
     * @return the api service
     */
    @Provides
    @Singleton
    ApiService provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(ApiService.class);
    }

    /**
     * Provides movies databse movie database.
     *
     * @param application the application
     * @return the movie database
     */
    @Provides
    @Singleton
    MovieDatabase providesMoviesDatabse(Application application) {
        return Room.databaseBuilder(application, MovieDatabase.class, "movie.db").build();
    }

    /**
     * Provide movie dao movie dao.
     *
     * @param movieDatabase the movie database
     * @return the movie dao
     */
    @Provides
    @Singleton
    MovieDao provideMovieDao(MovieDatabase movieDatabase) {
        return movieDatabase.movieDao();
    }

}
