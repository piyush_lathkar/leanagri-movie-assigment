package com.leanagri.moviedb.di.builder;

import com.leanagri.moviedb.view.fragment.MovieDetailsFragment;
import com.leanagri.moviedb.view.fragment.MovieListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Module
public abstract class FragmentBuilderModule {

    /**
     * Contribute movie list fragment movie list fragment.
     *
     * @return the movie list fragment
     */
    @SuppressWarnings("unused")
    @ContributesAndroidInjector
    abstract MovieListFragment contributeMovieListFragment();

    /**
     * Contribute movie detail fragment movie details fragment.
     *
     * @return the movie details fragment
     */
    @SuppressWarnings("unused")
    @ContributesAndroidInjector
    abstract MovieDetailsFragment contributeMovieDetailFragment();

}
