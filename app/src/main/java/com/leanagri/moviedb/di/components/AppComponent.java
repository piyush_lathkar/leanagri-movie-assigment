package com.leanagri.moviedb.di.components;

import android.app.Application;

import com.leanagri.moviedb.LeanAgriMovieApplication;
import com.leanagri.moviedb.di.builder.ActivityBuilderModule;
import com.leanagri.moviedb.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Singleton
@Component(modules = {AppModule.class, AndroidInjectionModule.class, ActivityBuilderModule.class})
public interface AppComponent {

    /**
     * The interface Builder.
     */
    @Component.Builder
    interface Builder {
        /**
         * Application builder.
         *
         * @param application the application
         * @return the builder
         */
        @BindsInstance
        Builder application(Application application);

        /**
         * Build app component.
         *
         * @return the app component
         */
        AppComponent build();
    }

    /**
     * Inject.
     *
     * @param leanAgriMovieApplication the lean agri movie application
     */
    void inject(LeanAgriMovieApplication leanAgriMovieApplication);

}
