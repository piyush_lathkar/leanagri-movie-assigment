package com.leanagri.moviedb.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leanagri.moviedb.R;
import com.leanagri.moviedb.data.local.entity.MovieEntity;
import com.leanagri.moviedb.databinding.FragmentMovieDetailsBinding;
import com.leanagri.moviedb.view.activity.MainActivity;
import com.leanagri.moviedb.view.base.BaseFragment;
import com.leanagri.moviedb.viewmodel.MovieDetailsViewModel;
import com.squareup.picasso.Picasso;

import static com.leanagri.moviedb.data.remote.ApiConstants.POSTER_BASE_URL;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class MovieDetailsFragment extends BaseFragment<MovieDetailsViewModel, FragmentMovieDetailsBinding> {

    /**
     * New instance movie details fragment.
     *
     * @param movieEntity the movie entity
     * @return the movie details fragment
     */
    public static MovieDetailsFragment newInstance(MovieEntity movieEntity) {
        Bundle args = new Bundle();
        args.putSerializable("movieEntity", movieEntity);
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Class<MovieDetailsViewModel> getViewModel() {
        return MovieDetailsViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_movie_details;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        MovieEntity movieEntity = (MovieEntity) getArguments().getSerializable("movieEntity");
        setMovieData(movieEntity);

        return dataBinding.getRoot();
    }

    private void setMovieData(MovieEntity movieEntity) {
        ((MainActivity) getActivity()).setToolbarTitle(movieEntity.getOriginalTitle().toUpperCase());
        Picasso.get().load(POSTER_BASE_URL + movieEntity.getPosterPath()).into(dataBinding.ivMovie);

        dataBinding.tvMovieName.setText(movieEntity.getOriginalTitle());
        dataBinding.tvAvgVote.setText(String.format("%s votes", String.valueOf(movieEntity.getVoteCount())));
        dataBinding.tvReleaseDate.setText(String.valueOf(movieEntity.getReleaseDate()));
        dataBinding.tvMovieDesc.setText(movieEntity.getOverview());
        if (movieEntity.getAdult())
            dataBinding.adultCheckTextView.setCheckMarkDrawable(android.R.drawable.checkbox_on_background);
        else
            dataBinding.adultCheckTextView.setCheckMarkDrawable(android.R.drawable.checkbox_off_background);

    }
}
