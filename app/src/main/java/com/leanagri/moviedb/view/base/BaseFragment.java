package com.leanagri.moviedb.view.base;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * The type Base fragment.
 *
 * @param <V> the type parameter
 * @param <D> the type parameter
 */
public abstract class BaseFragment<V extends ViewModel, D extends ViewDataBinding> extends Fragment {

    /**
     * The View model factory.
     */
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    /**
     * The View model.
     */
    protected V viewModel;

    /**
     * The Data binding.
     */
    protected D dataBinding;

    /**
     * Gets view model.
     *
     * @return the view model
     */
    protected abstract Class<V> getViewModel();

    /**
     * Gets layout res.
     *
     * @return the layout res
     */
    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        return dataBinding.getRoot();
    }

}
