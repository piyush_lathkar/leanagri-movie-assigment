package com.leanagri.moviedb.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import com.leanagri.moviedb.data.local.entity.MovieEntity;
import com.leanagri.moviedb.databinding.ItemMovieBinding;
import com.leanagri.moviedb.view.base.BaseAdapter;
import com.leanagri.moviedb.view.callbacks.MovieListItemClickCallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.leanagri.moviedb.data.remote.ApiConstants.POSTER_BASE_URL;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class MovieListAdapter extends BaseAdapter<MovieListAdapter.MovieViewHolder, MovieEntity> {

    private List<MovieEntity> movieEntities;

    private MovieListItemClickCallback movieListItemClickCallback;

    /**
     * Instantiates a new Movie list adapter.
     *
     * @param movieListItemClickCallback the movie list item click callback
     */
    public MovieListAdapter(@NonNull MovieListItemClickCallback movieListItemClickCallback) {
        movieEntities = new ArrayList<>();
        this.movieListItemClickCallback = movieListItemClickCallback;
    }

    @Override
    public void setData(List<MovieEntity> data) {
        movieEntities = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return MovieViewHolder.create(LayoutInflater.from(viewGroup.getContext()), viewGroup, movieListItemClickCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int i) {
        movieViewHolder.onBind(movieEntities.get(i));
    }

    @Override
    public int getItemCount() {
        return movieEntities.size();
    }

    /**
     * The type Movie view holder.
     */
    static class MovieViewHolder extends RecyclerView.ViewHolder {

        private static MovieViewHolder create(LayoutInflater inflater, ViewGroup parent, MovieListItemClickCallback callback) {
            ItemMovieBinding itemMovieBinding = ItemMovieBinding.inflate(inflater, parent, false);
            return new MovieViewHolder(itemMovieBinding, callback);
        }

        /**
         * The Binding.
         */
        final ItemMovieBinding binding;

        private MovieViewHolder(ItemMovieBinding binding, MovieListItemClickCallback callback) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(v ->
                    callback.onMovieClicked(binding.getMovie()));
        }

        private void onBind(MovieEntity movieEntity) {
            binding.setMovie(movieEntity);
            Picasso.get().load(POSTER_BASE_URL + movieEntity.getPosterPath()).into(binding.imageView);
            binding.executePendingBindings();
        }
    }
}
