package com.leanagri.moviedb.view.base;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * The type Base adapter.
 *
 * @param <T> the type parameter
 * @param <D> the type parameter
 */
public abstract class BaseAdapter<T extends RecyclerView.ViewHolder, D> extends RecyclerView.Adapter<T> {

    /**
     * Sets data.
     *
     * @param data the data
     */
    public abstract void setData(List<D> data);

}
