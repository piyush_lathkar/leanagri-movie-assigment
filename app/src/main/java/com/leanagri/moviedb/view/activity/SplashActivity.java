package com.leanagri.moviedb.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.leanagri.moviedb.R;
import com.leanagri.moviedb.databinding.ActivitySplashBinding;
import com.leanagri.moviedb.view.base.BaseActivity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends BaseActivity<ActivitySplashBinding> {

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(() -> startActivity(new Intent(SplashActivity.this, MainActivity.class)), 2000);
    }
}
