package com.leanagri.moviedb.view.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.leanagri.moviedb.R;
import com.leanagri.moviedb.databinding.ActivityMainBinding;
import com.leanagri.moviedb.utils.FragmentUtils;
import com.leanagri.moviedb.view.base.BaseActivity;
import com.leanagri.moviedb.view.fragment.MovieDetailsFragment;
import com.leanagri.moviedb.view.fragment.MovieListFragment;

import static com.leanagri.moviedb.utils.FragmentUtils.TRANSITION_NONE;

/**
 * The type Main activity.
 */
public class MainActivity extends BaseActivity<ActivityMainBinding> {

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(dataBinding.toolbar);

        FragmentUtils.replaceFragment(this, MovieListFragment.newInstance(), R.id.fragContainer, false, TRANSITION_NONE);

        setClickListeners();

        Toast.makeText(this, "Pull down to refresh", Toast.LENGTH_SHORT).show();
    }

    private void setClickListeners() {
        dataBinding.ivFilter.setOnClickListener(v -> showSortDialog());
    }

    /**
     * Sets toolbar title.
     *
     * @param title the title
     */
    public void setToolbarTitle(String title) {
        dataBinding.tvTitle.setText(title);
        dataBinding.ivFilter.setVisibility(View.GONE);
    }

    private void showSortDialog() {

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MovieListFragment.class.getCanonicalName());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog alertDialog = builder.create();

        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_filter, null);

        dialogView.findViewById(R.id.sort_by_date).setOnClickListener(view -> {
            if (fragment != null && fragment instanceof MovieListFragment) {
                MovieListFragment movieListFragment = (MovieListFragment) fragment;
                movieListFragment.performSortOnDate(getString(R.string.sort_by_date));
                alertDialog.dismiss();
            }
        });

        dialogView.findViewById(R.id.sort_by_rating).setOnClickListener(view -> {
            if (fragment != null && fragment instanceof MovieListFragment) {
                MovieListFragment movieListFragment = (MovieListFragment) fragment;
                movieListFragment.performSortOnDate(getString(R.string.sort_by_rating));
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MovieDetailsFragment.class.getCanonicalName());

        assert fragment != null;

        if (fragment instanceof MovieDetailsFragment) {
            setToolbarTitle(getString(R.string.app_title));
            dataBinding.ivFilter.setVisibility(View.VISIBLE);
        }

        super.onBackPressed();
    }
}
