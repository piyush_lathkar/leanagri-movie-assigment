package com.leanagri.moviedb.view.callbacks;

import com.leanagri.moviedb.data.local.entity.MovieEntity;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public interface MovieListItemClickCallback {
    /**
     * On movie clicked.
     *
     * @param movieEntity the movie entity
     */
    void onMovieClicked(MovieEntity movieEntity);
}
