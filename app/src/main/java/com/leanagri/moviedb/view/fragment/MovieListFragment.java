package com.leanagri.moviedb.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leanagri.moviedb.R;
import com.leanagri.moviedb.common.EndlessRecyclerViewScrollListener;
import com.leanagri.moviedb.data.local.entity.MovieEntity;
import com.leanagri.moviedb.data.remote.Status;
import com.leanagri.moviedb.databinding.FragmentMovieListBinding;
import com.leanagri.moviedb.utils.FragmentUtils;
import com.leanagri.moviedb.view.adapter.MovieListAdapter;
import com.leanagri.moviedb.view.base.BaseFragment;
import com.leanagri.moviedb.view.callbacks.MovieListItemClickCallback;
import com.leanagri.moviedb.viewmodel.MovieListViewModel;


/**
 * The type Movie list fragment.
 */
public class MovieListFragment extends BaseFragment<MovieListViewModel, FragmentMovieListBinding> implements MovieListItemClickCallback {

    /**
     * New instance movie list fragment.
     *
     * @return the movie list fragment
     */
    public static MovieListFragment newInstance() {
        Bundle args = new Bundle();
        MovieListFragment fragment = new MovieListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Class<MovieListViewModel> getViewModel() {
        return MovieListViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_movie_list;
    }

    @Override
    public void onMovieClicked(MovieEntity movieEntity) {
        if (getActivity() != null) {
            MovieDetailsFragment movieDetailsFragment = MovieDetailsFragment.newInstance(movieEntity);
            FragmentUtils.replaceFragment((AppCompatActivity) getActivity(), movieDetailsFragment, R.id.fragContainer, true, FragmentUtils.TRANSITION_SLIDE_LEFT_RIGHT);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        observeForDataChanges();
    }

    private void observeForDataChanges() {
        viewModel.getMovies().observe(this, listResource -> {

            if (dataBinding.swipeRefreshLayout.isRefreshing())
                dataBinding.swipeRefreshLayout.setRefreshing(false);

            if (listResource != null && (listResource.status == Status.SUCCESS || listResource.status == Status.ERROR)) {
                dataBinding.progressbar.setVisibility(View.GONE);
            }
            dataBinding.setResource(listResource);

            if (null != dataBinding.recyclerView.getAdapter() && dataBinding.recyclerView.getAdapter().getItemCount() > 0) {
                dataBinding.errorText.setVisibility(View.GONE);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        //RecyclerView Setup
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        dataBinding.recyclerView.setLayoutManager(gridLayoutManager);
        dataBinding.recyclerView.setAdapter(new MovieListAdapter(this::onMovieClicked));
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (page < 4) {
                    page = page + 1;
                    dataBinding.progressbar.setVisibility(View.VISIBLE);
                    viewModel.loadMoviesForGivenPage(page);
                    observeForDataChanges();
                }
            }
        };
        dataBinding.recyclerView.addOnScrollListener(scrollListener);

        //Refresh Data by swiping down
        dataBinding.swipeRefreshLayout.setOnRefreshListener(() -> observeForDataChanges());

        return dataBinding.getRoot();
    }

    /**
     * Perform sort on date.
     *
     * @param action the action
     */
    public void performSortOnDate(String action) {
        if (action.equalsIgnoreCase(getString(R.string.sort_by_rating))) {
            viewModel.getMoviesByRating().observe(this, resource -> dataBinding.setResource(resource));
        } else if (action.equalsIgnoreCase(getString(R.string.sort_by_date))) {
            viewModel.getMoviesByDate().observe(this, resource -> dataBinding.setResource(resource));
        }
    }
}
