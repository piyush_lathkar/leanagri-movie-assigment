package com.leanagri.moviedb.utils;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class DataConverter {

    /**
     * From string list.
     *
     * @param value the value
     * @return the list
     */
    @TypeConverter
    public static List<Integer> fromString(String value) {
        Type listType = new TypeToken<List<Integer>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    /**
     * From array list string.
     *
     * @param list the list
     * @return the string
     */
    @TypeConverter
    public static String fromArrayList(List<Integer> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }

}
