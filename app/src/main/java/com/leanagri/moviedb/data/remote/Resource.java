package com.leanagri.moviedb.data.remote;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.leanagri.moviedb.data.remote.Status.ERROR;
import static com.leanagri.moviedb.data.remote.Status.LOADING;
import static com.leanagri.moviedb.data.remote.Status.SUCCESS;

/**
 * Created By mayurlathkar on 11,May,2019
 *
 * @param <T> the type parameter
 */
public class Resource<T> {

    /**
     * The Status.
     */
    @NonNull
    public final Status status;

    /**
     * The Data.
     */
    @Nullable
    public final T data;

    @Nullable private final String message;

    private Resource(@NonNull Status status, @Nullable T data, @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    /**
     * Success resource.
     *
     * @param <T>  the type parameter
     * @param data the data
     * @return the resource
     */
    public static <T> Resource<T> success(@NonNull T data) {
        return new Resource<>(SUCCESS, data, null);
    }

    /**
     * Error resource.
     *
     * @param <T>  the type parameter
     * @param msg  the msg
     * @param data the data
     * @return the resource
     */
    public static <T> Resource<T> error(String msg, @Nullable T data) {
        return new Resource<>(ERROR, data, msg);
    }

    /**
     * Loading resource.
     *
     * @param <T>  the type parameter
     * @param data the data
     * @return the resource
     */
    public static <T> Resource<T> loading(@Nullable T data) {
        return new Resource<>(LOADING, data, null);
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    @Nullable
    public String getMessage() {
        return message;
    }

}
