package com.leanagri.moviedb.data.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.leanagri.moviedb.data.local.entity.MovieEntity;

import java.util.List;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Dao
public interface MovieDao {
    /**
     * Load movies live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM movie")
    LiveData<List<MovieEntity>> loadMovies();

    /**
     * Gets movie by vote count des.
     *
     * @return the movie by vote count des
     */
    @Query("SELECT * FROM movie ORDER BY voteAverage DESC")
    LiveData<List<MovieEntity>> getMovieByVoteCountDes();

    /**
     * Gets movie by vote count asc.
     *
     * @return the movie by vote count asc
     */
    @Query("SELECT * FROM movie ORDER BY voteAverage ASC")
    LiveData<List<MovieEntity>> getMovieByVoteCountAsc();

    /**
     * Gets movie by name.
     *
     * @return the movie by name
     */
    @Query("SELECT * FROM movie ORDER BY title")
    LiveData<List<MovieEntity>> getMovieByName();

    /**
     * Gets movie by release date.
     *
     * @return the movie by release date
     */
    @Query("SELECT * FROM movie ORDER BY release_date")
    LiveData<List<MovieEntity>> getMovieByReleaseDate();

    /**
     * Clear data.
     */
    @Query("DELETE FROM movie")
    void clearData();

    /**
     * Save movies.
     *
     * @param movieEntities the movie entities
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveMovies(List<MovieEntity> movieEntities);

    /**
     * Insert movie.
     *
     * @param movie the movie
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertMovie(MovieEntity movie);
}
