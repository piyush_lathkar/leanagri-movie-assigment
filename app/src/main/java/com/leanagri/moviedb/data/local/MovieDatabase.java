package com.leanagri.moviedb.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.leanagri.moviedb.data.local.dao.MovieDao;
import com.leanagri.moviedb.data.local.entity.MovieEntity;

/**
 * Created By mayurlathkar on 11,May,2019
 */
@Database(entities = {MovieEntity.class}, version = 2)
public abstract class MovieDatabase extends RoomDatabase {
    /**
     * Movie dao movie dao.
     *
     * @return the movie dao
     */
    public abstract MovieDao movieDao();
}
