package com.leanagri.moviedb.data.remote.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.leanagri.moviedb.data.local.dao.MovieDao;
import com.leanagri.moviedb.data.local.entity.MovieEntity;
import com.leanagri.moviedb.data.remote.ApiService;
import com.leanagri.moviedb.data.remote.NetworkBoundResource;
import com.leanagri.moviedb.data.remote.Resource;
import com.leanagri.moviedb.data.remote.model.MoviesResponse;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.leanagri.moviedb.data.remote.ApiConstants.API_KEY;
import static com.leanagri.moviedb.data.remote.ApiConstants.LANGUAGE;
import static com.leanagri.moviedb.data.remote.ApiConstants.REGION_FOR;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class MoviesRepository {
    private final MovieDao movieDao;
    private final ApiService apiService;

    /**
     * Instantiates a new Movies repository.
     *
     * @param dao     the dao
     * @param service the service
     */
    @Inject
    MoviesRepository(MovieDao dao, ApiService service) {
        this.movieDao = dao;
        this.apiService = service;
    }

    /**
     * Load movies live data.
     *
     * @param page     the page
     * @param api_key  the api key
     * @param language the language
     * @param region   the region
     * @return the live data
     */
    public LiveData<Resource<List<MovieEntity>>> loadMovies(int page, String api_key, String language, String region) {
        return new NetworkBoundResource<List<MovieEntity>, MoviesResponse>() {

            @Override
            protected void saveCallResult(MoviesResponse item) {
                if (null != item) {
                    insertAllMovies(item.getMovieDataList());
                }
            }

            @NonNull
            @Override
            protected LiveData<List<MovieEntity>> loadFromDb() {
                return movieDao.loadMovies();
            }

            @NonNull
            @Override
            protected Call<MoviesResponse> createCall() {
                return apiService.getMoviesList(page, API_KEY, language, region);
            }
        }.getAsLiveData();
    }

    /**
     * Load movies for given page.
     *
     * @param page     the page
     * @param api_key  the api key
     * @param language the language
     * @param region   the region
     */
    public void loadMoviesForGivenPage(int page, String api_key, String language, String region) {
        apiService.getMoviesList(page, API_KEY, language, region).enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.isSuccessful()) {
                    insertAllMovies(response.body().getMovieDataList());
                }
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {

            }
        });
    }

    /**
     * Insert all movies.
     *
     * @param list the list
     */
    public void insertAllMovies(List<MovieEntity> list) {
        new Thread(() -> {
            for (MovieEntity movieEntity : list)
                movieDao.insertMovie(movieEntity);
        }).start();
    }

    /**
     * Gets movies sort by rating.
     *
     * @return the movies sort by rating
     */
    public LiveData<Resource<List<MovieEntity>>> getMoviesSortByRating() {
        return new NetworkBoundResource<List<MovieEntity>, MoviesResponse>() {
            @Override
            protected void saveCallResult(MoviesResponse item) {
                if (item != null) {
                    insertAllMovies(item.getMovieDataList());
                }
            }

            @NonNull
            @Override
            protected LiveData<List<MovieEntity>> loadFromDb() {
                return movieDao.getMovieByReleaseDate();
            }

            @NonNull
            @Override
            protected Call<MoviesResponse> createCall() {
                return apiService.getMoviesList(1, API_KEY, LANGUAGE, REGION_FOR);
            }
        }.getAsLiveData();
    }

    /**
     * Gets movies sort by date.
     *
     * @return the movies sort by date
     */
    public LiveData<Resource<List<MovieEntity>>> getMoviesSortByDate() {
        return new NetworkBoundResource<List<MovieEntity>, MoviesResponse>() {
            @Override
            protected void saveCallResult(MoviesResponse item) {
                if (item != null) {
                    insertAllMovies(item.getMovieDataList());
                }
            }

            @NonNull
            @Override
            protected LiveData<List<MovieEntity>> loadFromDb() {
                return movieDao.getMovieByVoteCountAsc();
            }

            @NonNull
            @Override
            protected Call<MoviesResponse> createCall() {
                return apiService.getMoviesList(1, API_KEY, LANGUAGE, REGION_FOR);
            }
        }.getAsLiveData();
    }
}
