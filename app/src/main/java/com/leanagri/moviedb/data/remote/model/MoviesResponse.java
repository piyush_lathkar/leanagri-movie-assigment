package com.leanagri.moviedb.data.remote.model;

import com.google.gson.annotations.SerializedName;
import com.leanagri.moviedb.data.local.entity.MovieEntity;

import java.util.List;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class MoviesResponse {

    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("page")
    private int currentPage;

    @SerializedName("results")
    private List<MovieEntity> movieDataList;

    /**
     * Gets total pages.
     *
     * @return the total pages
     */
    public int getTotalPages() {
        return totalPages;
    }

    /**
     * Sets total pages.
     *
     * @param totalPages the total pages
     */
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Gets movie data list.
     *
     * @return the movie data list
     */
    public List<MovieEntity> getMovieDataList() {
        return movieDataList;
    }

    /**
     * Sets movie data list.
     *
     * @param movieDataList the movie data list
     */
    public void setMovieDataList(List<MovieEntity> movieDataList) {
        this.movieDataList = movieDataList;
    }


    /**
     * Gets current page.
     *
     * @return the current page
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

}
