package com.leanagri.moviedb.data.remote;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class ApiConstants {
    /**
     * The constant BASE_URL.
     */
    public static final String BASE_URL = "https://api.themoviedb.org/3/";

    /**
     * The constant CONNECT_TIMEOUT.
     */
    public static final long CONNECT_TIMEOUT = 30000;
    /**
     * The constant READ_TIMEOUT.
     */
    public static final long READ_TIMEOUT = 30000;
    /**
     * The constant WRITE_TIMEOUT.
     */
    public static final long WRITE_TIMEOUT = 30000;

    /**
     * The constant API_KEY.
     */
    public static final String API_KEY = "443233abaa2ede7f6e7676d8494e7a7b";
    /**
     * The constant MOVIE_LIST.
     */
    public static final String MOVIE_LIST = "movie/now_playing";

    /**
     * The constant LANGUAGE.
     */
    public static String LANGUAGE = "en-US";
    /**
     * The constant REGION_FOR.
     */
    public static String REGION_FOR = "IN";
    /**
     * The constant POSTER_BASE_URL.
     */
    public static String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w185";

    private ApiConstants() {
        // Private constructor to hide the implicit one
    }
}
