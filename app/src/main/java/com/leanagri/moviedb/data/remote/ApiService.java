package com.leanagri.moviedb.data.remote;

import com.leanagri.moviedb.data.remote.model.MoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.leanagri.moviedb.data.remote.ApiConstants.MOVIE_LIST;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public interface ApiService {

    /**
     * Gets movies list.
     *
     * @param page     the page
     * @param apiKey   the api key
     * @param language the language
     * @param region   the region
     * @return the movies list
     */
    @GET(MOVIE_LIST)
    Call<MoviesResponse> getMoviesList(@Query("page") int page, @Query("api_key") String apiKey, @Query("language") String language, @Query("region") String region);

}
