package com.leanagri.moviedb.data.remote;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public enum Status {
    /**
     * Success status.
     */
    SUCCESS,
    /**
     * Error status.
     */
    ERROR,
    /**
     * Loading status.
     */
    LOADING
}
