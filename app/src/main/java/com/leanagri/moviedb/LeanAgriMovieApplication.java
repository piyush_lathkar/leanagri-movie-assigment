package com.leanagri.moviedb;

import android.app.Activity;
import android.app.Application;

import com.leanagri.moviedb.di.components.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class LeanAgriMovieApplication extends Application implements HasActivityInjector {

    private static LeanAgriMovieApplication sInstance;


    /**
     * Gets app context.
     *
     * @return the app context
     */
    public static LeanAgriMovieApplication getAppContext() {
        return sInstance;
    }


    private static synchronized void setInstance(LeanAgriMovieApplication app) {
        sInstance = app;
    }

    /**
     * The Activity dispatching injector.
     */
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeComponent();
        setInstance(this);
    }

    private void initializeComponent() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingInjector;
    }
}
