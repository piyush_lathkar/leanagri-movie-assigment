package com.leanagri.moviedb.viewmodel;

import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class MovieDetailsViewModel extends ViewModel {

    /**
     * Instantiates a new Movie details view model.
     */
    @Inject
    public MovieDetailsViewModel() {

    }

}
