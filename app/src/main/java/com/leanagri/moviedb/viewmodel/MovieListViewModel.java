package com.leanagri.moviedb.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.leanagri.moviedb.data.local.entity.MovieEntity;
import com.leanagri.moviedb.data.remote.Resource;
import com.leanagri.moviedb.data.remote.repository.MoviesRepository;

import java.util.List;

import javax.inject.Inject;

import static com.leanagri.moviedb.data.remote.ApiConstants.API_KEY;
import static com.leanagri.moviedb.data.remote.ApiConstants.LANGUAGE;
import static com.leanagri.moviedb.data.remote.ApiConstants.REGION_FOR;

/**
 * Created By mayurlathkar on 11,May,2019
 */
public class MovieListViewModel extends ViewModel {

    private MoviesRepository moviesRepository;

    private LiveData<Resource<List<MovieEntity>>> movies;

    /**
     * Instantiates a new Movie list view model.
     *
     * @param moviesRepository the movies repository
     */
    @Inject
    public MovieListViewModel(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
        movies = moviesRepository.loadMovies(1, API_KEY, LANGUAGE, REGION_FOR);
    }

    /**
     * Load movies for given page.
     *
     * @param currentPage the current page
     */
    public void loadMoviesForGivenPage(int currentPage) {
        movies = moviesRepository.loadMovies(currentPage, API_KEY, LANGUAGE, REGION_FOR);
//        moviesRepository.loadMoviesForGivenPage(currentPage, API_KEY, LANGUAGE, REGION_FOR);
    }

    /**
     * Gets movies.
     *
     * @return the movies
     */
    public LiveData<Resource<List<MovieEntity>>> getMovies() {
        return movies;
    }

    /**
     * Gets movies by date.
     *
     * @return the movies by date
     */
    public LiveData<Resource<List<MovieEntity>>> getMoviesByDate() {
        return moviesRepository.getMoviesSortByDate();
    }

    /**
     * Gets movies by rating.
     *
     * @return the movies by rating
     */
    public LiveData<Resource<List<MovieEntity>>> getMoviesByRating() {
        return moviesRepository.getMoviesSortByRating();
    }
}
